shinyUI(fluidPage(
  tags$head(tags$link(rel='stylesheet', type='text/css', href='styles.css')),
  leafletMap(
    "map", "100%", 500,
#     initialTileLayer = "//{s}.tiles.mapbox.com/v3/jcheng.map-5ebohr46/{z}/{x}/{y}.png",
#     initialTileLayer = "http://{s}.tiles.mapbox.com/v3/examples.map-vyofok3q/{z}/{x}/{y}.png",
    initialTileLayerAttribution = HTML('Maps by <a href="http://www.mapbox.com/">Mapbox</a>'),
    options=list(
      center = c(-28.81, -52.58),
      zoom = 8,
      maxBounds = list(list(-17, -180), list(-59, 180))
    )
  ),
  fluidRow(
    column(8, offset=3,
           h2('População das cidades do Rio Grande do Sul'),
           htmlWidgetOutput(
             outputId = 'desc',
             HTML(paste(
               'Este mapa está centralizado em <span id="lat"></span>, <span id="lng"></span>',
               'com nível de zoom de <span id="zoom"></span>.<br/>',
               'Top <span id="shownCities"></span> de <span id="totalCities"></span> cidades apresentadas no mapa.'
             ))
           )
    )
  ),
  hr(),
  fluidRow(
    column(3,
#            selectInput('year', 'Ano', c(2000:2010), 2010),
            selectInput('year', 'Ano', 2010, 2010),
           selectInput('maxCities', 'Número máximo de cidades', choices=c(
             5, 25, 50, 100, 200, 500, 2000, 5000, 10000, All = 100000
           ), selected = 5)
    ),
    column(4,
           h4('Cidades apresentadas no mapa'),
           tableOutput('data')
    ),
    column(5,
           h4(id='cityTimeSeriesLabel', class='shiny-text-output'),
           plotOutput('cityTimeSeries', width='100%', height='250px')
    )
  )
))