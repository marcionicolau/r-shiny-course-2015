---
title: "Criando aplicações Web como R (shiny)"
author: Marcio Nicolau
date: 10 de junho de 2015
runtime: shiny
---

# Introdução 

## Resumo

- Analista 
    - Embrapa Trigo 
    - Passo Fundo 
    - RS
- Graduação em Estatistica (2006)
- MBA Eng. Produção (2015)
- Experiencias
    - Modelagem Bayesiana 
    - Simulação
    - Estatística Aplicada
    - Modelos estruturados
    - Programação (C++/Java/C/JS)
    - R
    - Computação Distribuída / GPU

## Agenda

- Preparar o ambiente de trabalho
    - Instalar o [R](http://www.r-project.org/), [RStudio](http://www.rstudio.com/products/rstudio/download/) e Bibliotecas
    - [Salvar repositório do curso](https://marcionicolau@bitbucket.org/marcionicolau/r-shiny-course-2015)
  - Introdução Web 2.0
    - CSS
    - HTML 5
    - Javascript
  - Pacotes de trabalho
    - [shiny](http://cran.r-project.org/web/packages/shiny/index.html) / [htmlwidgets](http://cran.r-project.org/web/packages/htmlwidgets/index.html) / [DT](http://cran.r-project.org/web/packages/DT/index.html)
    - [shinydashboard](https://github.com/rstudio/shinydashboard) / [networkD3](http://christophergandrud.github.io/networkD3/) / [sparkline](https://github.com/htmlwidgets/sparkline)
    - [rCharts](http://rcharts.io/) / [leaflet](https://github.com/rstudio/leaflet) / [dygraphs](http://cran.r-project.org/web/packages/dygraphs/index.html) 

## Formato do Curso

- Uso de tutoriais e documentos 
- Vignettes
- Dividido em períodos (4)
    - Intro (atual)
    - Web e suas ferramentas
    - Pacotes principais (básico) [shiny, htmlwidgets, shinydashboard, rCharts]
    - Pacotes adicionais (avançado) [DT, networkD3, sparkline, leaflet, dygraphs]
  
## Glossário e termos

- Web
    - HTML 5 (Ling Marcação para Web v5)
    - CSS (Folha de Estilo)
    - JS (Linguagem de programação executada no navegador)
- Bibliotecas
    - D3 (JS para dados dinâmicos)
    - Leaftlet (JS para mapas)
    - AngularJS (JS interface e extensão tags HTML 5)

# Dúvidas

## Durante o curso

- Levante a mão (a qualquer momento)

## Após o curso

- marcio.nicolau@embrapa.br
- skype: marcio.nicolau
- twitter: @marcionicolau
- URGENTE: (54) 3316-5933

# Considerações iniciais

## Participantes

- Conhecendo os participantes
- Expectativas
- Dados e/ou problemas para uso nos exemplos (período 4)
- Trabalhos com programação / web
- _Vamos iniciar?_


