---
title: "Criando aplicações Web como R (shiny)"
author: Marcio Nicolau
date: 10 de junho de 2015
runtime: shiny
---

# Sobre 

## Getting up

- Turn off alarm
- Get out of bed

## Breakfast

- Eat eggs
- Drink coffee

# In the evening

## Dinner

- Eat spaghetti
- Drink wine

## Other


```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "numericInput"
```

```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "renderTable"
```

## Novo


```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "sliderInput"
```

```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "renderPlot"
```



Resumo
========================================================
type: subsection
incremental: true

- Analista / Embrapa Trigo :: Passo Fundo :: RS
- Graduação em Estatistica (2006)
- MBA Eng. Produção (2015)
- Experiencias
  - Modelagem Bayesiana 
  - Simulação
  - Estatística Aplicada
  - Modelos estruturados
  - Programação (C++/Java/C/JS)
  - R


Agenda
========================================================
type: subsection
incremental: true

- Preparar o ambiente de trabalho
  - Instalar o [R](http://www.r-project.org/), [RStudio](http://www.rstudio.com/products/rstudio/download/) e Bibliotecas
  - [Salvar repositório do curso](https://marcionicolau@bitbucket.org/marcionicolau/r-ode-course-2015)
- Introdução ODE
- [CRAN Task View: Differential Equations](http://cran.r-project.org/web/views/DifferentialEquations.html)
- Pacotes de trabalho
  - [deSolve](http://cran.r-project.org/web/packages/deSolve/index.html) / [rootSolve](http://cran.r-project.org/web/packages/rootSolve/index.html) / [bvpSolve](http://cran.r-project.org/web/packages/bvpSolve/index.html)
  - [simecol](http://cran.r-project.org/web/packages/simecol/index.html)
  - [FME](http://cran.r-project.org/web/packages/FME/index.html)
  - [odeintr](http://cran.r-project.org/web/packages/odeintr/index.html)

Formato do Curso
========================================================
incremental: true

- O Curso será oferecido no format "lecture"
- Uso de tutoriais e documentos 
- Vignettes
- Dividido em períodos (4)
  - Intro (atual)
  - ODE (básico) com o pacote deSolve, rootSolve e bvpSolve: formulação, uso, gráficos e sumários dos dados
  - ODE (avançado) com os pacotes simecol, FME e odeintr
  - Exemplos e considerações finais
  
Glossário e termos
========================================================
incremental: true

- Modelagem
  - ODE (Eq. Diff. Ord.)
  - IVP (Prob. Valor Inic.)
  - BVP (Prob. Valor Cont.)
- Programas
  - git (Controlador de versão)
  - Boost (Biblioteca C++)
  - VexCL (Biblioteca GPU/CPU/OpenCL/C++)

Dúvidas
========================================================
incremental: true

- Durante o curso
  - Levante a mão (a qualquer momento)
- Após o curso
  - marcio.nicolau@embrapa.br
  - skype: marcio.nicolau
  - twitter: @marcionicolau
  - URGENTE: (54) 3316-5933

Considerações iniciais
========================================================
incremental: true
source: ../../R/deSolve.R 6

- Conhecendo os participantes
- Expectativas
- Dados e/ou problemas para uso nos exemplos (período 4)
- Trabalhos com programação / modelagem
- Linguagens de programação
- **Vamos iniciar?**





## Foo

It depends on who you ask.

Changing inputs can trigger *recalculations* and *side effects*.

The traditional solution for interactivity is callback oriented programming.

## Foo (cont...)



```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "reactive"
```

```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "reactive"
```

```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "renderPlot"
```





```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "flowLayout"
```

```r
radians <- reactive( input$degrees * pi / 180 )
```

```
## Error in eval(expr, envir, enclos): não foi possível encontrar a função "reactive"
```



